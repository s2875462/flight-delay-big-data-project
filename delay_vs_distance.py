from pyspark.sql import SparkSession
from pyspark.sql.functions import col, mean, abs, lit, min, max, desc, concat_ws, collect_set
from pyspark.sql.types import IntegerType, StringType

spark = SparkSession.builder.getOrCreate()

df = spark.read.parquet("/user/s2878755/airline.parquet")

df = df.select(col('Origin'), col('Dest'), col('Distance'), col('ArrDelay'), col('DepDelay')) \
        .filter(~((col('Distance') == 0) | col('Distance').contains('NA') | col('ArrDelay').contains('NA') | col('DepDelay').contains('NA') | col('Origin').contains('NA') | col('Dest').contains('NA') | col('Distance').isNull() | col('ArrDelay').isNull() | col('DepDelay').isNull() | col('Origin').isNull() | col('Dest').isNull())) \
        .select(col('Origin'), col('Dest'), col('Distance').cast(IntegerType()).alias('Distance'), col('ArrDelay'), col('DepDelay')) \
        .orderBy(col('Distance'))

df.write.csv("/user/s2878755/project/delay_vs_distance/delay_vs_distance.csv")

df_routes_1 = df.select(col('Origin'), col('Dest'), col('Distance'), col('ArrDelay'), col('DepDelay')) \
                .distinct() \
                .filter(col('Distance') > 3000)

df_routes_2 = df.select(col('Origin'), col('Dest'), col('Distance'), col('ArrDelay'), col('DepDelay')) \
                .filter(col('Distance') > 3000) \
                .groupBy(col('Origin'), col('Dest'), col('Distance')) \
                .agg(mean(col('ArrDelay')), mean(col('DepDelay')))

df_routes_1.write.csv("/user/s2878755/project/delay_vs_distance/routes_1.csv")
df_routes_2.write.csv("/user/s2878755/project/delay_vs_distance/routes_2.csv")


df_grouped = df.select(col('Distance'), (col('Distance')/100).cast(IntegerType()).alias('Group'), col('ArrDelay'), col('DepDelay')) \
                .groupBy(col('Group')) \
                .agg((min(col('Distance')).cast(StringType())).alias('min_dist'), (max(col('Distance')).cast(StringType())).alias('max_dist'), mean(col('ArrDelay')).alias('AverageArrDelay'), mean(col('DepDelay')).alias('AverageDepDelay')) \
                .select(col('group'), concat_ws('-',col('min_dist'),col('max_dist')).alias('Distance'), col('AverageArrDelay'), col('AverageDepDelay')) \
                .orderBy(col('group'))

df_grouped.write.csv("/user/s2878755/project/delay_vs_distance/delay_vs_distance_grouped.csv")

df_average = df.groupBy(col('Distance')) \
                .agg(mean(col('ArrDelay')).alias('AverageArrDelay'), mean(col('DepDelay')).alias('AverageDepDelay')) \
                .orderBy(col('Distance'))

df_average.write.csv("/user/s2878755/project/delay_vs_distance/avg_delay_vs_distance.csv")
