#project code
#group 3
#Run time for distributed mode with 10 executer is 2m39s
from pyspark import SparkContext
import pyspark.sql.functions as F
from pyspark.sql.functions import col, round
from pyspark.sql.window import Window
from pyspark.sql.session import SparkSession
from pyspark.sql.types import IntegerType

sc = SparkContext(appName="project")
sc.setLogLevel("ERROR")
spark = SparkSession(sc)

#read the dataset
#df = spark.read.csv("/user/s2875462/airline.csv.shuffle", header='true')
df = spark.read.parquet("/user/s2878755/airline.parquet")

#eleminating NA values
df=df.filter(col('WeatherDelay')!= 'NA')
df=df.filter(col('CarrierDelay')!= 'NA')
df=df.filter(col('NASDelay')!= 'NA')
df=df.filter(col('SecurityDelay')!= 'NA')
df=df.filter(col('LateAircraftDelay')!= 'NA')


#finding the percentage of cancelled flights
df_c=df.groupBy('Cancelled').count()
df_cancelled_vs_uncancelled=df_c\
     .withColumn('total', F.sum('count').over(Window.partitionBy()))\
     .withColumn ('percent', F.col('count')/F.col('total')*100)
#round the percentage     
df_cancelled_vs_uncancelled=df_cancelled_vs_uncancelled.select('Cancelled',round(col('percent'),2).alias('Cancelled_percentage'))
#save the output
#df_cancelled_vs_uncancelled.write.option("header",True).csv("file:/home/s2300397/Project_outputs")

#finding the proportion between different cancellation reasons
df1=df.select('FlightNum', 'Cancelled', 'CancellationCode')
df2=df1.filter(col('Cancelled')== 1)
df3=df2.filter(col('CancellationCode') != 'NA')

df4=df3.groupBy('CancellationCode').count()
#df_carrier=df4.filter(col('CancellationCode')== 'A')

df5=df4\
 .withColumn('total', F.sum('count').over(Window.partitionBy()))\
 .withColumn ('proportions', F.col('count')/F.col('total')*100)
df5=df5.select('CancellationCode',round(col('proportions'),2).alias('proportions'))
#df5.write.option("header",True).csv("file:/home/s2300397/Project_outputs_2")

#finding the percentage of diverted flights
df_d=df.select(col('FlightNum').alias('FN'), 'Cancelled', 'CancellationCode', 'Diverted')
df_d2=df_d.groupBy('Diverted').count()

df_d3=df_d2\
.withColumn('total', F.sum('count').over(Window.partitionBy()))\
.withColumn('percentage', F.col('count')/F.col('total')*100)
df_d3=df_d3.select('Diverted',round(col('percentage'),2).alias('Diverted_percentage'))
#df_d3.show()
#99.77% not diverted. 0.23% diverted

#how are diverted flights related to the cancellation reason?
df_diverted=df.select(col('FlightNum').alias('FN'), 'Diverted')
df_diverted=df_diverted.filter(col('Diverted')==1)
df_join=df_diverted.join(df2, col('FlightNum')== col('FN'))
df_join=df_join.filter(col('CancellationCode')!= 'NA')

df_join=df_join.groupBy('CancellationCode').count()
df_relation=df_join\
 .withColumn('total', F.sum('count').over(Window.partitionBy()))\
 .withColumn ('proportions', F.col('count')/F.col('total')*100)

df_relation=df_relation.select('CancellationCode',round(col('proportions'),2).alias('Diverted_cc_proportions'))
#df_relation.write.option("header",True).csv("file:/home/s2300397/Project_outputs_3")

#################
#arrival delay time vs departure delay time
df_arrival=df.select('FlightNum', 'ArrDelay', 'DepDelay')


#arrival delay time analysis
from pyspark.mllib.stat import Statistics
df_arrival=df_arrival.withColumn('ArrDelay', df_arrival['ArrDelay'].cast(IntegerType()))
df_arrival=df_arrival.withColumn('DepDelay', df_arrival['DepDelay'].cast(IntegerType()))
correlation=df_arrival.stat.corr("ArrDelay", "DepDelay")
print(str(correlation)) #the correlation is 0.86 which means that there is a positive strong correlation between departure delay and arrival delay



df_diff=df.select('DepDelay', 'WeatherDelay','CarrierDelay', 'NASDelay', 'SecurityDelay', 'LateAircraftDelay')
#check the data types
#df_diff.printSchema()

#convert from string to integer
df_diff=df_diff.withColumn('WeatherDelay', df_diff['WeatherDelay'].cast(IntegerType()))
df_diff=df_diff.withColumn('CarrierDelay', df_diff['CarrierDelay'].cast(IntegerType()))
df_diff=df_diff.withColumn('SecurityDelay', df_diff['SecurityDelay'].cast(IntegerType()))
df_diff=df_diff.withColumn('NASDelay', df_diff['NASDelay'].cast(IntegerType()))
df_diff=df_diff.withColumn('LateAircraftDelay', df_diff['LateAircraftDelay'].cast(IntegerType()))
df_diff=df_diff.withColumn('DepDelay', df_diff['DepDelay'].cast(IntegerType()))

df_diff=df_diff.filter(col('DepDelay')>0)

#linear regression
from pyspark.ml.feature import VectorAssembler
assembler = VectorAssembler(inputCols=['WeatherDelay','CarrierDelay','NASDelay','SecurityDelay','LateAircraftDelay'], outputCol='features')
data_set = assembler.transform(df_diff)
data_final=data_set.select("features", 'DepDelay')
#split into train and test data
train_data,test_data = data_final.randomSplit([0.7,0.3])
#Train the model (Fit the model with train data)
from pyspark.ml.regression import LinearRegression
lr = LinearRegression(labelCol='DepDelay')
lrModel = lr.fit(train_data)
#Perform descriptive analysis with correlation
test_stats = lrModel.evaluate(test_data)

print('Rsquared Error :',test_stats.r2)
#R2 value shows accuracy of model is 91% therefore the model accuracy is very good and can be use for predictive analysis
print(lrModel.coefficients)
#output:[0.924516685608,0.977890526462,0.584648891466,0.922487371013,0.984988102191]
print(lrModel.intercept)
#output: 4.99341543121
print(test_stats.rootMeanSquaredError)
#output: 14.3465317557
print(test_stats.meanSquaredError)
#output: 205.822973417


#------







