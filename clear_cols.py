from pyspark.sql import SparkSession

spark = SparkSession.builder.getOrCreate()

df = spark.read.csv("/user/s2875462/airline.csv.shuffle", header='true')

df = df.drop('DepTime').drop('CRSDepTime').drop('ArrTime').drop('CRSArrTime').drop('UniqueCarrier').drop('TailNum').drop('AirTime').drop('taxiIn').drop('taxiOut')

df.write.parquet('/user/s2878755/airline.parquet')
