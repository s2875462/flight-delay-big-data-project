from pyspark.sql import SparkSession
from pyspark.sql.functions import col
#from pyspark.ml.classification import LogisticRegression
from pyspark.ml.feature import VectorAssembler

spark = SparkSession.builder.getOrCreate()
#df = spark.read.csv("/user/s2875462/airline.csv.shuffle", header="true")
df = spark.read.parquet("/user/s2878755/airline.parquet")

'''
QUESTION: Is there a certain threshold where the total delay causes flights to be canceled?
Local run:
Cluster run:

Approach:
    1. Get all flights which don't have a NA value for departure delay, with cols depdelay and cancelled
    2. Fit a logistic regression model to do a binary classification (cancelled/not cancelled) based on the total delay (depdelay)
    3. Analyze the interception of the model and see if there's some correlation.


Output:
'''

df2 = df.select(col("DepDelay").cast("int"), col("Cancelled").cast("int"))\
         .where(col("DepDelay") >= 0)
#         .sort(col("DepDelay"))
assembler = VectorAssembler(inputCols=["DepDelay"], outputCol="features")
transformed_df = assembler.transform(df2)
data = transformed_df.select("features", "Cancelled")
train_data, test_data = data.randomSplit([0.7, 0.3])

from pyspark.ml.regression import LinearRegression

lr = LinearRegression(labelCol="Cancelled")
lrModel = lr.fit(train_data)
test_stats = lrModel.evaluate(test_data)

print("R2 error:", test_stats.r2)
print("Coefficients: ", lrModel.coefficients)
print("Intercept: ", lrModel.intercept)


#tot = df2.count()

#lr = LogisticRegression(maxIter=10, regParam=0.3, elasticNetParam=0.8)
#lrModel = lr.fit(df2)

#print("Coefficients: "+str(lrModel.coefficients))
#print("Intercept: "+str(lrModel.intercept))

#df3 = df2.limit(tot//4).select(pmax("DepDelay").alias("max"))
#df3.show()

#df4 = df2.limit(2*tot//4).select(pmax("DepDelay").alias("max"))
#df4.show()

#df5 = df2.limit(3*tot//4).select(pmax("DepDelay").alias("max"))
#df5.show()

#df6 = df2.select(pmax("DepDelay").alias("max"))
#df6.show()

#        .where((col("Cancelled") == 1) & (col("DepDelay") > 0))


df2.show(10)
#print(df2.count())


