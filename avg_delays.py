from pyspark.sql import SparkSession
from pyspark.sql.functions import col, explode 

spark = SparkSession.builder.getOrCreate()
#df = spark.read.csv("/user/s2875462/airline.csv.shuffle", header="true") 
df = spark.read.parquet("/user/s2878755/airline.parquet")

'''
QUESTION: What is the average delay time when it is caused by security reasons vs when it is caused by weather conditions?
Local run: 6m50.311s
Cluster run: 2m29.112s 

Output:
    The average delay caused by security is 20 minutes.
    The average delay caused by weather conditions is 43 minutes.
'''

'''
   --- Average Security Delay ---
'''
df_s = df.select(col("SecurityDelay")).where(col("SecurityDelay") > 0)
sum_s = df_s.rdd.map(lambda x: (1, x[0])).reduceByKey(lambda x, y: int(x) + int(y)).collect()[0][1]
avg_s = sum_s / df_s.count()
print("The average delay caused by security is "+str(avg_s)+" minutes.")


'''
   --- Average Weather Delay ---
'''
df_w = df.select(col("WeatherDelay")).where(col("WeatherDelay") > 0)
sum_w = df_w.rdd.map(lambda x: (1, x[0])).reduceByKey(lambda x, y: int(x) + int(y)).collect()[0][1]
avg_w = sum_w / df_w.count()
print("The average delay caused by weather conditions is "+str(avg_w)+" minutes.")

'''
   --- Average Arrival Delay ---
'''
df_arr = df.select(col("ArrDelay")).where(col("ArrDelay") > 0)
sum_arr = df_arr.rdd.map(lambda x: (1, x[0])).reduceByKey(lambda x, y: int(x) + int(y)).collect()[0][1]
avg_arr = sum_arr / df_arr.count()
print("The average arrival delay is "+str(avg_arr)+" minutes.")


'''
   --- Average Departure Delay ---
'''
df_dep = df.select(col("DepDelay")).where(col("DepDelay") > 0)
sum_dep = df_dep.rdd.map(lambda x: (1, x[0])).reduceByKey(lambda x, y: int(x) + int(y)).collect()[0][1]
avg_dep = sum_dep / df_dep.count()
print("The average departure delay is "+str(avg_dep)+" minutes.")
 
'''
   --- Average Carrier Delay ---
'''
df_car = df.select(col("CarrierDelay")).where(col("CarrierDelay") > 0)
sum_car = df_arr.rdd.map(lambda x: (1, x[0])).reduceByKey(lambda x, y: int(x) + int(y)).collect()[0][1]
avg_car = sum_car / df_car.count()
print("The average carrier delay caused is "+str(avg_car)+" minutes.")

'''
   --- Average NAS Delay ---
'''
df_nas = df.select(col("NASDelay")).where(col("NASDelay") > 0)
sum_nas = df_nas.rdd.map(lambda x: (1, x[0])).reduceByKey(lambda x, y: int(x) + int(y)).collect()[0][1]
avg_nas = sum_nas / df_nas.count()
print("The average NAS delay is "+str(avg_nas)+" minutes.")

'''
   --- Average Late Aircraft Delay ---
'''
df_late = df.select(col("LateAircraftDelay")).where(col("LateAircraftDelay") > 0)
sum_late = df_late.rdd.map(lambda x: (1, x[0])).reduceByKey(lambda x, y: int(x) + int(y)).collect()[0][1]
avg_late = sum_late / df_late.count()
print("The average late aircraft delay is "+str(avg_late)+" minutes.")

